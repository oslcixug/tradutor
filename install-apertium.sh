#!/bin/bash

echo "Hola! Este es el script de instalación de Apertium"
echo "Debería ser ejecutado con root"
echo "Es necesario tener acceso al repositorio de git."

echo "Vamos a actualizar e instalar los paquetes necesarios"
sudo apt update
sudo apt install git apertium apertium-apy apertium-es-gl apertium-es-en apertium-gl-en  -y


echo "Clonamos el repositorio"
git clone https://github.com/apertium/apertium-html-tools.git

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add

echo "Actualizamos e instalamos el npm y el yarn"
apt update
apt install npm yarn -y


curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -

echo "Instalamos el nodejs"
apt install nodejs -y

cd cixug-apertium

echo "Instalaremos las dependencias necesarias"

yarn install --dev; yarn build; python3 -m http.server

echo "Puede acceder a la URL http://127.0.0.1:8000"

