import { Config, Mode } from './src/types';

export default {
  defaultLocale: 'glg',
  htmlUrl: 'https://beta.apertium.org/',
  apyURL: 'https://trad20apy.codery.net',

  ALLOWED_LANGS: ['es', 'gl', 'en', 'cat', 'por'],
  ALLOWED_VARIANTS: ['es', 'gl', 'en', 'cat', 'por'],

  defaultMode: Mode.Translation,
  enabledModes: new Set([Mode.Translation]),
  translationChaining: true,

  subtitle: 'Beta',
  subtitleColor: 'rgb(220, 41, 38)',

  stringReplacements: {
    '{{maintainer}}': "<a href='https://wiki.apertium.org/wiki/Apertium' target='_blank' rel='noopener'>Apertium</a>",
  },
} as Config;
