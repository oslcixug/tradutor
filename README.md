# Apertium CIXUG

## Dependencias

El despliegue de una interfaz web para apertium requiere:

1. Desplegar el contenido de este repositorio(apertium-html-tools)
2. Disponer de **apertium-apy** para que la interfaz web pueda enviar peticiones a este servicio web.

### Despliegue apertium-html-tools

**Instalación de yarn**

`echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`

`curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add` 

**Instalación de nodejs**

`apt install npm -y`

`curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -`

`apt install nodejs -y`

**Configuración entorno**

Una vez clonado este repositorio es necesario editar el fichero config.ts e indicar en la propiedad **apyURL** la URL del servicio apertium-apy. Ejemplo:

`apyURL: 'https://trad20apy.cixug.gal',`

A continuación instalaremos las dependencias necesarias para construir en entorno web ejecutando sobre el directorio raíz de este repositorio:

`yarn install --dev`

En el siguiente paso construiremos el contenido estátido para el sitio:

`yarn build`

Como último paso ejecutaremos el script *configurar.sh* que copiará desde el directorio **/assets** los ficheros CSS y HTML e imágenes que incluyen la personalización de CIXUG para **apertium-html-tools**:

`chmod +x configurar.sh; ./configurar.sh`

En el directorio **/dist** se encuentra el código fuente que debe servir desde su servidor web. Ejemplo para Apache 2.4

```
<VirtualHost *:80>
	ServerAdmin osl@cixug.es
	DocumentRoot /var/www/html/dist
	DirectoryIndex index.glg.html
	##
	<IfModule mod_headers.c>
		Header set Access-Control-Allow-Origin "FQDN-apertium-apy"

		Header add Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"
	</IfModule>
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
``` 


### Despliegue apertium-apy

Como primer paso, actualizaremos las fuentes de repositorio e instalaremos los siguientes paquetes:

`apt update; apt install -y apertium-apy apertium apertium-en-es apertium-en-gl apertium-es-gl`

Una vez instalado, es necesario asegurarse que el servicio está iniciado y se ejecuta el al iniciar el servidor:

`sudo systemctl start apertium-apy`

`sudo systemctl enable apertium-apy`

El contexto de instalación habitual es configurar **apertium-apy** detrás de un proxy inverso. Ejemplo para Nginx:

```
server {
    listen 1.2.3.4:443 ssl http2;
    ssl_certificate /root/example.crt;
    ssl_certificate_key /root/example.key;


    server_name trad20.cixug.gal;
	

    access_log /var/log/nginx/access_apy.log;
    error_log /var/log/nginx/error_apy.log;	


    location / {
	      proxy_pass http://localhost:2737;
        proxy_set_header host $host;
        proxy_set_header X-real-ip $remote_addr;
        proxy_set_header X-forward-for $proxy_add_x_forwarded_for;
   }

}

```
El valor de la directia **server_name** en este ejemplo **trad20.cixug.gal** será el valor de la propiedad **apyURL** del fichero config.ts.
