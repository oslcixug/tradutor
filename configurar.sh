#!/bin/bash

echo ""
echo "Copiar fichero css"

cp assets/css/index.css dist/index.css
echo ""

echo "Copiar plantillas HTML"

cp assets/html/index.glg.html dist/index.glg.html
cp assets/html/index.spa.html dist/index.spa.html
cp assets/html/index.eng.html dist/index.eng.html
echo ""

echo "Copiar fuentes"
cp assets/fonts/* dist/
echo ""

echo "Copiar imagenes"
cp assets/img/* dist/
echo ""
echo "Fin del proceso"

python3 -m http.server 8082
